import styled from "styled-components"

export const Container = styled.div`
  width: 100vw;
  margin-bottom: 20px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  .CardContainer {
    box-sizing: border-box;
    width: 55vw;
    border-radius: 20px;
    margin: 0 auto 90px;
    padding: 30px;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
  }

  .ButtonsContainer {
    width: 50vw;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .favPage {
    margin-top: 20px;
    padding: 10px 15px;
    border-radius: 5px;
    background: #300660;
    color: #c2a0f8;
    transition: 0.2s;
    :hover {
      cursor: pointer;
      background: #500880;
    }
  }

  @media (max-width: 768px) {
    .CardContainer {
      width: 100vw;
    }
  }
`
