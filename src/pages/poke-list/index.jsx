import { Container } from "./style"
import { Left, Right } from "../../components/buttons"
import { useState, useEffect } from "react"
import Card from "../../components/card"
import axios from "axios"
import { Link } from "react-router-dom"
import { motion } from "framer-motion"

const PokemonList = ({ favIncludes }) => {
  const [pokemons, setPokemons] = useState([])
  const [offset, setOffset] = useState(0)
  const [cardMark, setCardMark] = useState([])

  const handleFavorite = (id) => {
    let char = pokemons.filter(
      (pokemon) => pokemon.url === `https://pokeapi.co/api/v2/pokemon/${id}/`
    )

    favIncludes("pokemon", char)
  }

  const handlePagePrev = () => {
    if (offset === 0) {
      return
    }

    setOffset(offset - 20)
  }

  const handlePageNext = () => {
    if (offset >= 150) {
      return
    }

    setOffset(offset + 20)
  }

  const getPokemons = () => {
    axios
      .get(`https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=20`)
      .then((res) => setPokemons(res.data.results))
      .catch((err) => console.error(err))
  }

  useEffect(getPokemons, [offset])

  const markFavorite = (id) => {
    setCardMark([...cardMark, id])
  }

  return (
    <motion.div
      initial={{ x: -100 }}
      animate={{ x: 0 }}
      end={{ x: 100 }}
      transition={{ duration: 1 }}
    >
      <Container>
        <div className="CardContainer">
          {pokemons.map((pokemon, index) => {
            const brokenUrl = pokemon.url.split("/")
            const id = brokenUrl[brokenUrl.length - 2]

            return (
              <Card
                key={index}
                id={id}
                theme="secondary"
                name={pokemon.name}
                imgUrl={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`}
                handleFavorite={handleFavorite}
                markFavorite={markFavorite}
                mark={cardMark.includes(parseInt(id))}
              />
            )
          })}
        </div>
        <div className="ButtonsContainer">
          <Left onClick={handlePagePrev} />
          <Right onClick={handlePageNext} />
        </div>
        <Link className="favPage" to="/pokemon-favorites">
          Ir para Favoritos
        </Link>
      </Container>
    </motion.div>
  )
}

export default PokemonList
