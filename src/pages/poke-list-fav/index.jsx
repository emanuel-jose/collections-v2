import { Container } from "./style"
import { Link } from "react-router-dom"
import { BiArrowBack } from "react-icons/bi"
import Card from "../../components/card"
import { motion } from "framer-motion"

const PokemonFavorites = ({ list, favRemove }) => {
  const handleRemove = (ev) => {
    let card = list.filter((item) => {
      const brokenUrl = item.url.split("/")
      const id = brokenUrl[brokenUrl.length - 2]

      return id === ev.target.id
    })

    favRemove("pokemon", card[0].name)
  }

  return (
    <motion.div
      initial={{ x: -100 }}
      animate={{ x: 0 }}
      end={{ x: 100 }}
      transition={{ duration: 1 }}
    >
      <Container>
        <div className="cards">
          {list.map((pokemon, index) => {
            const brokenUrl = pokemon.url.split("/")
            const id = brokenUrl[brokenUrl.length - 2]

            return (
              <div key={index} className="card">
                <Card
                  type="fav"
                  theme="secondary"
                  name={pokemon.name}
                  imgUrl={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`}
                />
                <button className="remove" id={id} onClick={handleRemove}>
                  Remover
                </button>
              </div>
            )
          })}
        </div>
        <Link to="/pokemon">
          <BiArrowBack />
        </Link>
      </Container>
    </motion.div>
  )
}

export default PokemonFavorites
