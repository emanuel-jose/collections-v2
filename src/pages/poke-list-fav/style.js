import styled from "styled-components"

export const Container = styled.div`
  width: 100vw;
  height: 90vh;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  .cards {
    box-sizing: border-box;
    width: 70vw;
    border-radius: 20px;
    margin: 0 auto;
    padding: 30px;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
  }

  .card {
    text-align: center;
  }

  .remove {
    background: red;
    border-radius: 3px;
    padding: 5px 10px;
    color: #fff;
    text-transform: uppercase;
    font-weight: 700;
    font-size: 10px;
    transition: 0.2s;

    :hover {
      cursor: pointer;
      background: darkred;
    }
  }

  a {
    font-size: 40px;
  }

  @media (max-width: 768px) {
    .cards {
      width: 100vw;
    }
  }
`
