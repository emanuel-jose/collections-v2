import { Container } from "./style"
// import Search from "../../components/search"
import { Left, Right } from "../../components/buttons"
import { useState, useEffect } from "react"
import Card from "../../components/card"
import axios from "axios"
import { Link } from "react-router-dom"
import { motion } from "framer-motion"

const RickAndMortyList = ({ favIncludes }) => {
  const [characters, setCharacters] = useState([])
  const [page, setPage] = useState(1)
  const [info, setInfo] = useState(0)
  const [pageUrl, setPageUrl] = useState(
    `https://rickandmortyapi.com/api/character/?page=${page}`
  )
  const [cardIds, setCardIds] = useState([])

  const modifyPageUrl = () => {
    setPageUrl(`https://rickandmortyapi.com/api/character/?page=${page}`)
  }

  const handlePagePrev = () => {
    if (page === 1) {
      return
    }

    setPage(page - 1)
  }

  const handlePageNext = () => {
    if (page === info) {
      return
    }

    setPage(page + 1)
  }

  const getCharacters = () => {
    axios
      .get(pageUrl)
      .then((res) => {
        setCharacters(res.data.results)
        setInfo(res.data.info.pages)
      })
      .catch((err) => console.error(err))
  }

  useEffect(getCharacters, [pageUrl])
  useEffect(modifyPageUrl, [page])

  const handleFavorite = (id) => {
    let char = characters.filter((character) => character.id === id)

    favIncludes("rick", char)
  }

  const markFavorite = (id) => {
    setCardIds([...cardIds, id])
  }

  return (
    <motion.div
      initial={{ x: -100 }}
      animate={{ x: 0 }}
      end={{ x: 100 }}
      transition={{ duration: 1 }}
    >
      <Container>
        {/* <Search /> */}
        <div className="CardContainer">
          {characters.map((character, index) => (
            <Card
              key={index}
              theme="primary"
              id={character.id}
              name={character.name}
              imgUrl={character.image}
              handleFavorite={handleFavorite}
              markFavorite={markFavorite}
              mark={cardIds.includes(character.id)}
            />
          ))}
        </div>
        <div className="ButtonsContainer">
          <Left onClick={handlePagePrev} />
          <Right onClick={handlePageNext} />
        </div>
        <Link className="favPage" to="/rickandmorty-favorites">
          Ir para Favoritos
        </Link>
      </Container>
    </motion.div>
  )
}

export default RickAndMortyList
