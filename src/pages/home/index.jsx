import { Container } from "./style"
import { Link } from "react-router-dom"
import { CgPokemon } from "react-icons/cg"
import { GiAtom } from "react-icons/gi"
import { motion } from "framer-motion"

const Home = () => {
  return (
    <motion.div
      initial={{ scale: 0.9 }}
      animate={{ scale: 1 }}
      exit={{ scale: 0.9 }}
      transition={{ duration: 1 }}
    >
      <Container>
        <h1>Card Collections</h1>
        <div className="LinkContainer">
          <Link to="/rickandmorty">
            <GiAtom />
            <p>Rick and Morty</p>
          </Link>
          <Link to="/pokemon">
            <CgPokemon />
            <p>Pokemon</p>
          </Link>
        </div>
      </Container>
    </motion.div>
  )
}

export default Home
