import styled from "styled-components"

export const Container = styled.div`
  width: 80vw;
  height: 90vh;
  margin: 0 auto;
  background-color: #000;
  border: 5px solid #300660;
  border-radius: 20px;
  margin-top: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;

  h1 {
    width: 100%;
    font-size: 80px;
    text-align: center;
    color: #560bad;
    font-family: "Nunito", sans-serif;
    font-weight: 700;
  }

  .LinkContainer {
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  a {
    font-size: 50px;
    color: #560bad;
    margin: 0 25px;
    text-align: center;
    transition: 0.2s;

    p {
      font-size: 16px;
      font-weight: 700;
    }
  }

  a:hover {
    color: #7d19f0;
  }

  @media (max-width: 768px) {
    height: 70vh;

    h1 {
      font-size: 50px;
    }
  }
`
