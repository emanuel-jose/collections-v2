import { Container } from "./style"
import { Link } from "react-router-dom"
import { BiArrowBack } from "react-icons/bi"
import Card from "../../components/card"
import { motion } from "framer-motion"

const RickAndMortyFavorites = ({ list, favRemove }) => {
  const handleRemove = (ev) => {
    favRemove("rick", parseInt(ev.target.id))
  }

  return (
    <motion.div
      initial={{ x: -100 }}
      animate={{ x: 0 }}
      end={{ x: 100 }}
      transition={{ duration: 1 }}
    >
      <Container>
        <div className="cards">
          {list.map((listItem, index) => (
            <div key={index} className="card">
              <Card
                type="fav"
                name={listItem.name}
                imgUrl={listItem.image}
                theme="primary"
              />
              <button
                className="remove"
                id={listItem.id}
                onClick={handleRemove}
              >
                Remover
              </button>
            </div>
          ))}
        </div>
        <Link to="/rickandmorty">
          <BiArrowBack />
        </Link>
      </Container>
    </motion.div>
  )
}

export default RickAndMortyFavorites
