import { Container } from "./style"
import { AiFillStar } from "react-icons/ai"
import { motion } from "framer-motion"

const Card = ({
  id,
  name,
  imgUrl,
  handleFavorite,
  theme,
  type,
  mark,
  markFavorite,
}) => {
  const handleClick = (ev) => {
    if (type === "fav") {
      return
    }
    let id = parseInt(ev.currentTarget.id)
    markFavorite(id)
    handleFavorite(id)
  }

  return (
    <motion.div
      whileHover={{
        y: -10,
        scale: 1.2,
        zIndex: 10,
        transition: { duration: 0.5 },
      }}
      whileTap={{
        scale: 1,
      }}
    >
      <Container id={id} theme={theme} onClick={handleClick}>
        {mark && (
          <div className="star">
            <AiFillStar />
          </div>
        )}
        <img src={imgUrl} alt={name} />
        <p>{name}</p>
      </Container>
    </motion.div>
  )
}

export default Card
