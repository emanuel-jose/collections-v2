import styled, { css } from "styled-components"

export const Container = styled.button`
  position: relative;
  box-sizing: border-box;
  width: 130px;
  height: 150px;
  margin: 5px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  border-radius: 5px;
  border: 1px solid lightgreen;
  ${({ theme }) => {
    if (theme === "primary") {
      return css`
        background: green;
        color: lightgreen;
        :hover {
          background: #0a0;
          color: #334f00;
        }
      `
    }

    if (theme === "secondary") {
      return css`
        background: blue;
        color: lightblue;
        :hover {
          background: #00a;
          color: #00f3ff;
        }
      `
    }
  }}

  img {
    margin-top: 5px;
    width: 120px;
    height: 100px;
    border-radius: 10px;
  }

  p {
    margin-bottom: 5px;
  }

  div.star {
    font-size: 20px;
    color: goldenrod;
    position: absolute;
    top: 0;
    right: 0;
    transition: all 0.2s;
  }

  :hover {
    cursor: pointer;
  }
`
