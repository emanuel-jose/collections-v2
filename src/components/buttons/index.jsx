import { Container } from "./style"
import { FiChevronsLeft } from "react-icons/fi"
import { FiChevronsRight } from "react-icons/fi"

export const Right = ({ onClick }) => {
  return (
    <Container onClick={onClick}>
      <FiChevronsRight />
    </Container>
  )
}

export const Left = ({ onClick }) => {
  return (
    <Container onClick={onClick}>
      <FiChevronsLeft />
    </Container>
  )
}