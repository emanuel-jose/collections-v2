import styled from "styled-components"

export const Container = styled.button`
  padding: 5px 10px;
  font-size: 30px;
  border: 2px solid #462885;
  color: #462885;
  background: #130326;
  border-radius: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 2px;
  transition: 0.2s;

  :hover {
    cursor: pointer;
    background-color: #462885;
    color: #130326;
  }
`
