import styled from "styled-components"
import Menu from "@material-ui/core/Menu"

export const Container = styled.div`
  background-color: #300660;
  width: 100vw;
  height: 50px;

  a {
    text-decoration: none;
  }

  button span {
    font-size: 25px;
    color: #c2a0f8;
  }

  button span a {
    color: #c2a0f8;
  }

  #main-menu a {
    color: #300660;
  }
`

export const MenuContainer = styled(Menu)`
  .MuiPaper-root {
    margin-top: 35px;
    background-color: #300660;

    a {
      color: #c2a0f8;
      transition: 0.2s;

      :hover {
        color: #e7d9fc;
      }
    }
  }
`
