import { Container, MenuContainer } from "./style"
import { BiHomeAlt } from "react-icons/bi"
import { FiMenu } from "react-icons/fi"
import MenuItem from "@material-ui/core/MenuItem"
import Button from "@material-ui/core/Button"
import { Link } from "react-router-dom"
import { useState } from "react"

const MenuHeader = () => {
  const [anchor, setAnchor] = useState(null)

  const handleClick = (ev) => {
    setAnchor(ev.currentTarget)
  }

  const handleClose = () => {
    setAnchor(null)
  }

  return (
    <Container>
      <Button>
        <Link to="/">
          <BiHomeAlt />
        </Link>
      </Button>
      <Button
        aria-controls="main-menu"
        aria-haspopup="true"
        variant="outlined"
        onClick={handleClick}
      >
        <FiMenu />
      </Button>

      <MenuContainer
        id="main-menu"
        anchorEl={anchor}
        open={!!anchor}
        onClose={handleClose}
      >
        <MenuItem>
          <Link to="/rickandmorty">Rick and Morty Cards</Link>
        </MenuItem>
        <MenuItem>
          <Link to="/pokemon">Pokemon Cards</Link>
        </MenuItem>
        <MenuItem>
          <Link to="/rickandmorty-favorites">Rick and Morty Favorites</Link>
        </MenuItem>
        <MenuItem>
          <Link to="/pokemon-favorites">Pokemon Favorites</Link>
        </MenuItem>
      </MenuContainer>
    </Container>
  )
}

export default MenuHeader
