import { Route, Switch } from "react-router-dom"
import Home from "../pages/home"
import PokemonList from "../pages/poke-list"
import RickAndMortyList from "../pages/rick-list"
import PokemonFavorites from "../pages/poke-list-fav"
import RickAndMortyFavorites from "../pages/rick-list-fav"
import { useState, useEffect } from "react"
import { AnimatePresence } from "framer-motion"

const Routes = () => {
  const [favoriteList, setFavoriteLsit] = useState({
    pokemon: [],
    rick: [],
  })

  const favIncludes = (destination, char) => {
    const { pokemon, rick } = favoriteList
    if (destination === "pokemon") {
      let verify = pokemon.find((element) => element.name === char[0].name)
      if (verify) {
        return
      }
      setFavoriteLsit({
        pokemon: [...pokemon, ...char],
        rick: [...rick],
      })
    }

    if (destination === "rick") {
      let verify = rick.find((element) => element.id === char[0].id)
      if (verify) {
        return
      }
      setFavoriteLsit({
        pokemon: [...pokemon],
        rick: [...rick, ...char],
      })
    }
  }

  const favRemove = (listName, id) => {
    const { rick, pokemon } = favoriteList
    if (listName === "pokemon") {
      let indexCardToRemove = pokemon.findIndex(
        (element) => element.name === id
      )
      pokemon.splice(indexCardToRemove, 1)
      setFavoriteLsit({
        pokemon: pokemon,
        rick: rick,
      })
    }

    if (listName === "rick") {
      let indexCardToRemove = rick.findIndex((element) => element.id === id)

      rick.splice(indexCardToRemove, 1)
      setFavoriteLsit({
        pokemon: pokemon,
        rick: rick,
      })
    }
  }

  return (
    <AnimatePresence>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/rickandmorty">
          <RickAndMortyList favIncludes={favIncludes} />
        </Route>
        <Route exact path="/pokemon">
          <PokemonList favIncludes={favIncludes} />
        </Route>

        <Route exact path="/rickandmorty-favorites">
          <RickAndMortyFavorites
            list={favoriteList.rick}
            favRemove={favRemove}
          />
        </Route>

        <Route exact path="/pokemon-favorites">
          <PokemonFavorites list={favoriteList.pokemon} favRemove={favRemove} />
        </Route>
      </Switch>
    </AnimatePresence>
  )
}

export default Routes
