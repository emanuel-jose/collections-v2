import Routes from "./routes/index"
import MenuHeader from "./components/menu-header"
import GlobalStyle from "./styles/global"

const App = () => {
  return (
    <>
      <GlobalStyle />
      <MenuHeader />
      <Routes />
    </>
  )
}

export default App
