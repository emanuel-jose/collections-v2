import { createGlobalStyle } from "styled-components"

export default createGlobalStyle`
    html, body {
        margin: 0;
        padding: 0;
    }

    body {
        background-color: #130326;
    }

    p, h1, a, input, button {
        box-sizing: border-box;
        padding: 0;
        margin: 0;
        text-decoration: none;
        border: none;
        font-family: 'Roboto', sans-serif;
    }
`
